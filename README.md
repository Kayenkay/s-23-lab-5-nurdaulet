# Lab5 -- Integration testing

### Specs

Here is InnoCar Specs:
Budet car price per minute = 18
Luxury car price per minute = 40
Fixed price per km = 10
Allowed deviations in % = 15
Inno discount in % = 18

### Solution

Follow the [link](https://docs.google.com/spreadsheets/d/1aSgjBh4sg-30PbuoBJgHJNmaqmLkCzTjdu5-Zijg0sM/edit?usp=sharing) to see results.
Note that there are 2 pages with tables

### Conclusion

Calculations are almost never correct. It was really hard to get expected result. Discounts are totally ignored. I received first "ok" result when the type was "luxury". After that server passed few consequent requests. But then it failed. So, it is hard to in which conditions results are correct or not. Poor users

P.S. It doesn't relate to the current lab, but when getting InnoCar Specs it returns the word "Budet" instead of "Budget". Bug hunting point? :)